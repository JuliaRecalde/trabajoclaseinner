package com.company;

class OuterClass{
    //Inner classes


    private int x = 200;
    class InnerClass{
        private int ans;
        public void getValue(){
            display();
            System.out.println("valor de Outer class: " + x);
        }
    }
    public void display(){
        System.out.println("Display de Outer Class");
    }
    public void accessInner(){
        InnerClass obj = new InnerClass();
        obj.ans=500;
        System.out.println("Valor de Inner Class: "+ obj.ans);
    }


    //Method-local Inner Classes

    // Instanciar metodo de Outer Class

    void mi_Metodo() {
        int num = 23;

        // metodo-local Inner Class
        class MetodoInner_Demo {
            public void print() {
                System.out.println("Este es el metodo inner class "+num);
            }
        }

        // Accediendo a inner class
        MetodoInner_Demo inner = new MetodoInner_Demo();
        inner.print();
    }


    // Static Nested Classes
    static class Nested_Demo {
        public void my_method() {
            System.out.println("Esta es mi nested class");
        }
    }

}


public class Main {

    public static void main(String[] args) {

        //Para Inner classes

        OuterClass outobj = new OuterClass();
	    OuterClass.InnerClass inobj=outobj.new InnerClass();
	    inobj.getValue();
        

        //Para Method-local Inner Classes

        OuterClass outer = new OuterClass();
        outer.mi_Metodo();


        //Para Anonymous Inner Classes
        InnerAnonima inner = new InnerAnonima() {
            public void metodo() {
                System.out.println("Ejemplo de anonymous inner class");
            }
        };
        inner.metodo();


        //Para Static Nested Classes
        OuterClass.Nested_Demo nested = new OuterClass.Nested_Demo();
        nested.my_method();

    }
}
